import Link from 'next/link'

export default function Navbar(){
return(
    <header className='bg-deepblue ' >
        <nav className='flex w-full space-x-16 h-12'>
        <Link href="/" className="mr-6" >
          <a className="text-gray-light hover:text-red ">Главная</a>
        </Link>
        <Link href="/videocards" className="mr-6" >
          <a className="text-gray-light hover:text-red">Видеокарты</a>
        </Link>
        <Link href="/cart" className="mr-6" >
          <a className="text-gray-light hover:text-red">Корзина</a>
        </Link>
        </nav>
    </header>
)
}

