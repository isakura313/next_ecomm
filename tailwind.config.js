module.exports = {
  content: [    "./pages/**/*.{js,ts,jsx,tsx}",    "./components/**/*.{js,ts,jsx,tsx}",  ],
  theme: {
    screens: {
      'sm': '640px',
      'md': '768px',
      'lg': '1024px',
      'xl': '1280px',
      '2xl': '1536px',
    },
    fontFamily: {
      'sans': ['Roboto', 'system-ui'],
    },
    colors: {
      'deepblue': '#2C3753',
      'lightblue': '#0648EC',
      'secondary': '#179BB9',
      'red': '#EC2F06',
      'gray-light': '#d3dce6',
    },
  },
  plugins: [],
}
